package ru.t1.shevyreva.tm.exception.field;

import ru.t1.shevyreva.tm.exception.AbstractException;

public abstract class AbsrtactFieldException extends AbstractException {

    public AbsrtactFieldException() {
    }

    public AbsrtactFieldException(String message) {
        super(message);
    }

    public AbsrtactFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbsrtactFieldException(Throwable cause) {
        super(cause);
    }

    public AbsrtactFieldException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
