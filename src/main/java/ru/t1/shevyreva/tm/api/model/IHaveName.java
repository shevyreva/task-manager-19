package ru.t1.shevyreva.tm.api.model;

public interface IHaveName {

    String getName();

    void setName(String name);

}
