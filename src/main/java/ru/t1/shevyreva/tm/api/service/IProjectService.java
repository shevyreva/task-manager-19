package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project create(String name, String description);

    Project create(String name);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project changeProjectStatusByIndex(Integer Index, Status status);

    Project changeProjectStatusById(String id, Status status);

}
