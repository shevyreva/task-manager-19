package ru.t1.shevyreva.tm.api.repository;

import ru.t1.shevyreva.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name, String description);

    Project create(String name);

}
