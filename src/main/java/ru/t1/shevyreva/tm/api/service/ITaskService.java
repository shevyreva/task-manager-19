package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.enumerated.Sort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name, String description);

    Task create(String name);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId);

}
