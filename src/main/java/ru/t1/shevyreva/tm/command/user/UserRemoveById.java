package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserRemoveById extends AbstractUserCommand {

    private final String DESCRIPTION = "User remove by Id.";

    private final String NAME = "user-remove-by-id";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY ID]:");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        getUserService().removeById(id);
    }
}
