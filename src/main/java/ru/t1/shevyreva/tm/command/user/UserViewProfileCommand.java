package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "View user profile.";

    private final String NAME = "user-profile";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[USER PROFILE]");
        System.out.println("[USER ID]: " + user.getId());
        System.out.println("[LOGIN]: " + user.getLogin());
        System.out.println("[EMAIL]: " + user.getEmail());
        System.out.println("[FIRST NAME]: " + user.getFirstName());
        System.out.println("[LAST NAME]: " + user.getLastName());
        System.out.println("[MIDDLE NAME]: " + user.getMiddleName());
        System.out.println("[ROLE]: " + user.getRole());
    }

}
