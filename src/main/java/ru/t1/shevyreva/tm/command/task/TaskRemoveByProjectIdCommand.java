package ru.t1.shevyreva.tm.command.task;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskRemoveByProjectIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Remove task by project Id.";

    private final String NAME = "task-remove-by-project-id";


    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        //final Task task = getTaskService().findOneById(id);
        getProjectTaskService().removeByProjectId(id);
    }

}
