package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "Change password.";

    private final String NAME = "user-change-password";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        final String userId = getAuthService().getUserId();
        System.out.println("Enter new password:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}
