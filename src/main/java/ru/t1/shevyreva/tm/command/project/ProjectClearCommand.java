package ru.t1.shevyreva.tm.command.project;

public class ProjectClearCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Clear project.";

    private final String NAME = "project-clear";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear();
    }
}
