package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "User login.";

    private final String NAME = "login";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}
