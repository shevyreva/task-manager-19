package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserRemoveByEmail extends AbstractUserCommand {

    private final String DESCRIPTION = "User remove by email.";

    private final String NAME = "user-remove-by-email";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY EMAIL]:");
        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();
        getUserService().removeByEmail(email);
    }


}
