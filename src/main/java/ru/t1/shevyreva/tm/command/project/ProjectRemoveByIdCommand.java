package ru.t1.shevyreva.tm.command.project;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Remove project by Id.";

    private final String NAME = "project-remove-by-id";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        //final Project project = getProjectService().findOneById(id);
        getProjectService().removeById(id);
    }

}
