package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.service.IAuthService;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;
import ru.t1.shevyreva.tm.exception.field.LoginEmptyException;
import ru.t1.shevyreva.tm.exception.field.PasswordEmptyException;
import ru.t1.shevyreva.tm.exception.user.AccessDeniedException;
import ru.t1.shevyreva.tm.exception.user.IncorrectDataException;
import ru.t1.shevyreva.tm.exception.user.NeedLogoutException;
import ru.t1.shevyreva.tm.model.User;
import ru.t1.shevyreva.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    public void login(final String login, final String password) {
        if (isAuth()) throw new NeedLogoutException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectDataException();
        userId = user.getId();
    }

    public void logout() {
        userId = null;
    }

    private Boolean isAuth() {
        return userId != null;
    }

    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

}
