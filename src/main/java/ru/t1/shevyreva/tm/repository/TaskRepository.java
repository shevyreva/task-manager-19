package ru.t1.shevyreva.tm.repository;

import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> listTasks = new ArrayList<>();
        for (Task task : records) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId))
                listTasks.add(task);
        }
        return listTasks;
    }

}
